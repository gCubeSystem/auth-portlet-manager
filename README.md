# gCube System - Auth Manager Portlet 

The  Auth Manager Portlet  is a Liferay portlet that is functional to the Authorization layer of the infrastructure

## Structure of the project

* The source code is present in the src folder. 

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

* Use of this service is described in the [Wiki](https://wiki.gcube-system.org/gcube/User%27s_Guide).

## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/auth-portlet-manager/releases).

## Authors

* **Alessandro Pieve ** - [ISTI-CNR](

## Maintainers



## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)