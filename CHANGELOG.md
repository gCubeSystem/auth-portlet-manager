
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.1.0] - 2022-03-15

- fixed bug on policies
- ported to git
- ported to Java 8 and GWT 2.8.2

## [v1.0.0] - [2015-10-15]

- First Release